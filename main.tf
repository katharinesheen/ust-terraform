provider "aws" {
    region = "ap-south-1"
    access_key = "AKIA2HGPF37UINGC7LNL"
    secret_key = "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" 
}


variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}
variable my_ip {}

       
resource "aws_vpc" "my-app-vpc"{  
    cidr_block = var.vpc_cidr_block
    tags = {
        Name: "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "myapp-subnet-1"{
    vpc_id = aws_vpc.my-app-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name: "${var.env_prefix}-subnet-1"
    }
}
# route table, like a virtual router
resource "aws_internet_gateway" "myapp-igw"{
    vpc_id = aws_vpc.my-app-vpc.id 
    tags = {
        Name: "${var.env_prefix}-igw"
    }
}

# # internet gateway like vitual modem
# resource "aws_route_table" "myapp-route-table"{
#     vpc_id = aws_vpc.my-app-vpc.id
#     route{
#         cidr_block = "0.0.0.0/0"
#         gateway_id = aws_internet_gateway.myapp-igw.id
#     }
#     tags = {
#         Name: "${var.env_prefix}-rtb"
#     }
# }

# #subnt association with created route table(custom created)
# resource "aws_route_table_association" "a-rtb-subnet"{
#     subnet_id = aws_subnet.myapp-subnet-1.id
#     route_table_id = aws_route_table.myapp-route-table.id 
# }

resource "aws_default_route_table" "main-rtb"{
    default_route_table_id = aws_vpc.my-app-vpc.default_route_table_id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
    }
    tags = {
        Name: "${var.env_prefix}-main-rtb"
    }
}

# Ingress and Egress are Firewall Rules
#Creating Security Group
# For incoming traffic( - ssh into EC2 and access from browser)
# who is allowed to access resource on port 22 
# Ingress refers to the right to enter a property

resource "aws_security_group" "myapp-sg" {
    name = "myapp-sg"
    vpc_id = aws_vpc.my-app-vpc.id 

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }

# ( - installations eg: fetch Docker Image)
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
# For Outgoing Traffic( - installations eg: fetch Docker Image)
# Right to exit a property
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
    }

    tags = {
        Name: "${var.env_prefix}-sg"
    }

}





